using System;
using Xunit;

using calculaireConsole;

namespace calculaireTests
{
    public class UnitTest1
    {
        [Fact]
        public void TestAireCarre()
        {
            Assert.Equal(9.0, Program.aireCarre(3));
            Assert.Equal(16.0, Program.aireCarre(4));
        }
        [Fact]
        public void TestAireRectangle()
        {
            Assert.Equal(12.0, Program.aireRectagle(3,4));
            Assert.Equal(40.0, Program.aireRectagle(8,5));
        }
        [Fact]
        public void TestAireTriangle()
        {
            Assert.Equal(20.0, Program.aireTriangle(4,10));
            Assert.Equal(10.5, Program.aireTriangle(3,7));
        }
        [Fact]
        public void TestIsMajor()
        {
            Assert.True(Program.isMajor(2001));
            Assert.False(Program.isMajor(2004));
        }
    }
}
