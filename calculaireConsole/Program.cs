﻿using System;

namespace calculaireConsole
{
    public class Program
    {
        public static float aireCarre(float cote)
        {
            return cote*cote;
        }
        public static float aireRectagle(float largeur, float longueur)
        {
            return largeur*longueur;
        }
        public static float aireTriangle(float baseTriangle, float hauteur)
        {
            return (baseTriangle * hauteur)/2;
        }
        public static bool isMajor(int annee_naissance)
        {
            if(DateTime.Now.Year - annee_naissance < 18)
            {
                return false;
            }else{
                return true;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        
    }
}
